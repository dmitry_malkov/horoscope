//TODO: Источник для гороскопа (http://www.ignio.com/static/r/public/export/xml.html)

//TODO: План действий:
//TODO: ldpi (0.75x)    68х68
//      mdpi (1x)       90х90
//      hdpi (1.5x)     135x135
//      xhdpi (2.0x)    180x180
//      xxhdpi (3.0x)   270x270
//      xxxhdpi (4.0x)  320x320

//TODO: SELECT strftime('%Y.%m.%d %H:%M:%S', time/1000, 'unixepoch') from horoscope_period;

package com.example.malkov.horoscope;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Main extends AppCompatActivity implements ZodiacFragment.OnFragmentInteractionListener {
    final String TAG = "MyLog";
    final String TODAY = "today";

    Button btnAries, btnTaurus, btnGemini, btnCancer, btnLeo, btnVirgo, btnLibra, btnScorpio
            , btnSagittarius, btnCapricorn, btnAquarius, btnPisces;

    FragmentTransaction fTrans;
    ZodiacFragment frgZodiac;
    ForecastZodiacFragment frgZodiacForecast;

    Date date;

    DB db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final ActionBar bar = getSupportActionBar();
        // Кнопка назад в ActionBar
        bar.setDisplayHomeAsUpEnabled(true);

        // Фрагменты
        frgZodiac = new ZodiacFragment();
        frgZodiacForecast = new ForecastZodiacFragment();

        // Стартовый фрагмент
        fTrans = getSupportFragmentManager().beginTransaction();
        fTrans.add(R.id.frm, frgZodiac,"tag");
        fTrans.commit();

        // Текущая дата
        date = new Date();
        SimpleDateFormat fDate = new SimpleDateFormat("dd.MM.yyyy");
        date = stringToDate(fDate.format(date));

        // Подключенеи к базе
        db = DB.getInstance(this);

        // обновляем данные
        this.update();
    }

    /**
     * Проверяем наличие интернета
     *
     * @param context
     * @return
     */
    public static boolean hasConnection ( final Context context){
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting())
        {
            return true;
        }
        return false;
    }

    /**
     * Преобразуем строку в дату
     *
     * @param date
     * @return
     */
    public Date stringToDate(String date){
        Date docDate = null;
        try {
            SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
            docDate = format.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return docDate;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch (id) {
            case R.id.action_settings:
                this.update();
                return true;
            case android.R.id.home:
                this.onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
        // Закрываем подключение к базе
        db.close();
    }

    /**
     * Проваливаемся в прогноз гороскопа
     */
    private void toForecast(){
        fTrans = getSupportFragmentManager().beginTransaction();
        fTrans.addToBackStack(null);
        fTrans.replace(R.id.frm, frgZodiacForecast, "tag");
        fTrans.commit();
    }

    @Override
    public void onFragmentInteraction(String zodiac){
        frgZodiacForecast.setArguments(getBundleByNameZodiac(zodiac));
        toForecast();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    /**
     * Формируем данные
     *
     * @param zodiac
     * @return
     */
    private Bundle getBundleByNameZodiac(String zodiac){
        Bundle b = new Bundle();
        if(!zodiac.isEmpty()) {
            b.putString("ID", zodiac);
        }
        return b;
    }

    /**
     * обновляем данные
     */
    private void update(){
        String m;
        // Наличие интеренета
        if(!hasConnection(this)){
            m = "Нет соединения";
        }else if(this.db.findPeriodByCurrentDate(this.date).equals(this.TODAY)){ //  Актуальность данных
            m =  "Данные актуальны";
        }else{
            m = "Обновление данных";
            new HoroscopeDay(this).execute(db.getHoroscopeType());
        }
        Toast toast = Toast.makeText(getApplicationContext(),m,Toast.LENGTH_SHORT);
        toast.show();
    }
}
