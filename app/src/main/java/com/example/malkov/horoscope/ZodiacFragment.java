package com.example.malkov.horoscope;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;


/**
 * Стартовый экран
 * отображает плашки зодиаков
 */
public class ZodiacFragment extends Fragment {
    final String TAG = "MyLog";

    private OnFragmentInteractionListener mListener;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_zodiac, container, false);

        int[] idZodiacs = {R.id.aries,R.id.taurus,R.id.gemini,R.id.cancer,R.id.leo,R.id.virgo,
                R.id.libra,R.id.scorpio,R.id.sagittarius,R.id.capricorn,R.id.aquarius,R.id.pisces};

        // Нажатие на плашку зодиака
        for(int id: idZodiacs){
            ImageButton btn = (ImageButton) v.findViewById(id);
            btn.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.onFragmentInteraction(v.getTag().toString());
                    Log.d(TAG, v.getTag().toString());
                }
            });
        }
        return v;
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        //mListener = null;
    }

    public interface OnFragmentInteractionListener {
        public void onFragmentInteraction(String zodiac);
    }
}
