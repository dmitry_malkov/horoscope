package com.example.malkov.horoscope;

import android.database.Cursor;
import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by malkov on 21.09.2015.
 *
 * Коннектимся к серверу данных и получаем ответ
 */
public abstract class HoroscopeConnect extends AsyncTask<Cursor, Void,Void> {

    private final String TAG = "MyLog";

    /**
     * Коннект к серверу по url
     *
     * @param urlStr
     * @return
     */
    protected String downloadXML(String urlStr){
        HttpURLConnection httpUrlConn = null;
        InputStream stream = null;
        InputStreamReader isr = null;
        BufferedReader br = null;
        String file = null;

        try {
            URL url = new URL(urlStr);
            httpUrlConn = (HttpURLConnection) url.openConnection();
            stream = httpUrlConn.getInputStream();

            isr = new InputStreamReader(stream);
            br = new BufferedReader(isr);

            String line;
            StringBuffer fileBuf = new StringBuffer();
            while ((line = br.readLine()) != null) {
                fileBuf.append(line);
            }
            file = fileBuf.toString();
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            try {
                if (br != null) br.close();
                if(isr != null) isr.close();
                if(stream != null) stream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            httpUrlConn.disconnect();

        }
        return file;
    }
}
