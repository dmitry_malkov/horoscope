package com.example.malkov.horoscope;

import android.util.Xml;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.StringReader;
import java.util.HashMap;
import java.util.Map;

/**
 * Парсим XML
 *
 * Created by malkov on 23.09.2015.
 */
public class XmlParserHoroscopeDay {
    private final String TAG = "MyLog";

    private Map<String,Map<String,String>> zodiacs = new HashMap<String,Map<String,String>>();
    private Map<String,String> days;
    private Map<String,String> date;


    private static final String ns = null;

    /**
     * Начинаем парсить
     *
     * @param in
     * @throws XmlPullParserException
     * @throws IOException
     */
    public void parse(String in) throws XmlPullParserException, IOException{
        try {
            XmlPullParser parser = Xml.newPullParser();
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(new StringReader(in));
            readXML(parser);
        }finally {
        }
    }

    /**
     * Читаем XML
     *
     * @param parser
     * @throws XmlPullParserException
     * @throws IOException
     */
    private void readXML (XmlPullParser parser) throws XmlPullParserException, IOException{
        String text = null;
        int event = parser.getEventType();

        while(event != XmlPullParser.END_DOCUMENT){
            switch (parser.getEventType()) {
                case XmlPullParser.START_TAG:
                    fillHoroscope(parser);
                    break;
                case XmlPullParser.TEXT:
                    text = parser.getText();
                    break;
                case XmlPullParser.END_TAG:
                    break;
            }

            event = parser.next();
        }
    }

    /**
     * Обрабатываем знаки зодиака и дату
     *
     * @param parser
     * @throws XmlPullParserException
     * @throws IOException
     */
    private void fillHoroscope(XmlPullParser parser) throws XmlPullParserException, IOException {
        String name = parser.getName();
        switch (name) {
            case "date":
                zodiacs.put(name,fillDataHoroscope(parser));
                break;
            case "aries":
            case "taurus":
            case "gemini":
            case "cancer":
            case "leo":
            case "virgo":
            case "libra":
            case "scorpio":
            case "sagittarius":
            case "capricorn":
            case "aquarius":
            case "pisces":
                zodiacs.put(name,fillDayHoroscope(parser));
                break;
        }
    }

    /**
     * Обрабатываем гороскоп по дням
     *
     * @param parser
     * @throws XmlPullParserException
     * @throws IOException
     */
    private Map<String,String> fillDayHoroscope(XmlPullParser parser) throws XmlPullParserException, IOException {
        days = new HashMap<String,String>();
        while (parser.nextTag() != XmlPullParser.END_TAG) {
            String text = parser.nextText();
            String name = parser.getName();
            days.put(name, text);
        }
        return days;
    }

    /**
     * обрабатываем дату
     *
     * @param parser
     * @throws XmlPullParserException
     * @throws IOException
     */
    private Map<String, String> fillDataHoroscope(XmlPullParser parser) throws XmlPullParserException, IOException {
        parser.require(XmlPullParser.START_TAG, ns, "date");
        date = new HashMap<String, String>();
        for (int i = 0; i < parser.getAttributeCount(); i++) {
            String name = parser.getAttributeName(i);
            String value = parser.getAttributeValue(i);
            date.put(name, value);
        }
        return date;
    }

    /**
     * Возвращаем гороскоп по зодиакам
     *
     * @return
     */
    public Map<String,Map<String,String>> getZodiacs(){
        return zodiacs;
    }

}