package com.example.malkov.horoscope;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.database.Cursor;
import android.util.Log;
import org.xmlpull.v1.XmlPullParserException;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;


/**
 * Created by malkov on 21.09.2015.
 *
 * Коннектимся к серверу данных и получаем ответ
 */
public class HoroscopeDay extends HoroscopeConnect {

    Main mainActivity;
    DB myDB;

    private int idHorType ;
    private Map<String, Date> dates = new HashMap<String, Date>();

    ProgressDialog pd = null;

    public HoroscopeDay(Main main) {
        mainActivity = main;
        myDB = DB.getInstance(main);
    }

    @Override
    protected void onPreExecute(){
        // Диологовое окно загрузки
        this.pd = new ProgressDialog(this.mainActivity);
        this.pd.setMessage(this.mainActivity.getString(R.string.dialog_message));
        this.pd.setCancelable(false);
        this.pd.show();
        super.onPreExecute();
    }


    protected Void doInBackground(Cursor... cursors) {
        Cursor c = cursors[0];
        String strServerResponse = null;
        boolean isData = false;
        if(c.moveToFirst()){
            do{
                idHorType = 0;
                int id = c.getColumnIndex(myDB.C_ID);
                int idName = c.getColumnIndex(myDB.C_NAME);
                int idURL = c.getColumnIndex(myDB.C_URL);
                idHorType = c.getInt(id);
                String url = c.getString(idURL);

                XmlParserHoroscopeDay xmlParser = new XmlParserHoroscopeDay();
                strServerResponse = downloadXML(url);
                if (strServerResponse != null) {
                    try {
                        xmlParser.parse(strServerResponse);
                        fillHoroscope(xmlParser.getZodiacs());
                    } catch (XmlPullParserException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    isData = true;
                }

            }while (c.moveToNext());
        }
        if(isData) fillTimeHoroscopePeriod(dates);

        return null;
    }

    @Override
    protected void onPostExecute(Void result){
        if(this.pd != null){
            this.pd.hide();
        }
        super.onPostExecute(result);
    }

    /**
     * Разбираем коллекцию и заносим данные в таблицу
     *
     * @param zodiacs
     */
    protected void fillHoroscope(Map zodiacs) {
        Iterator<Map<String, Map<String, String>>> et = zodiacs.entrySet().iterator();
        while (et.hasNext()) {
            int idZodiacs;
            Map.Entry<String, Map<String, String>> entry = (Map.Entry) et.next();
            String zodiac = entry.getKey();
            Map<String, String> map = (Map) entry.getValue();
            idZodiacs = myDB.findIDByNameZodiacs(zodiac);

            for (Map.Entry<String, String> ent : map.entrySet()) {
                String period = ent.getKey();
                String text = ent.getValue();
                int idHorPeriod = myDB.findIDByNameHoroscopePeriod(period);
                if(zodiac.equals("date") && dates.size() < 4 ){
                    switch (period){
                        case "yesterday":
                        case "today":
                        case "tomorrow":
                        case "tomorrow02":
                            dates.put(period, mainActivity.stringToDate(text));
                            break;
                    }
                }else if(!zodiac.equals("date")){
                    // Заносим данные в Horoscope
                    ContentValues cv = new ContentValues();
                    cv.put(myDB.C_ID_ZODIACS, idZodiacs);
                    cv.put(myDB.C_HOR_PERIOD, idHorPeriod);
                    cv.put(myDB.C_HOR_TYPE, idHorType);
                    cv.put(myDB.C_TEXT, text);
                    myDB.replace(myDB.T_HOROSCOPE, null, cv);
                }
            }
        }
    }

    /**
     * Заполняем время в HoroscopePeriod
     *
     * @param dates
     */
    private void fillTimeHoroscopePeriod(Map dates){
        myDB.myDB.beginTransaction();
        Log.i(mainActivity.TAG, "--setTimeHoroscopePeriod");
        Iterator<Map<String, Date>> et = dates.entrySet().iterator();
        while (et.hasNext()){
            Map.Entry<String, Date> entry = (Map.Entry) et.next();
            String period = entry.getKey();
            Date date = (Date) entry.getValue();
            Log.i(mainActivity.TAG, "---period = " + period + " ---date = " + date.getTime()/10000);

            ContentValues cv = new ContentValues();
            cv.put(myDB.C_TIME, date.getTime()/1000);
            String selection = myDB.C_PERIOD + " = ? ";
            String[] selectionArgs = new String[]{period};
            myDB.myDB.update(myDB.T_HOROSCOPE_PERIOD,cv,selection,selectionArgs);
        }
        myDB.myDB.setTransactionSuccessful();
        myDB.myDB.endTransaction();
    }
}
