package com.example.malkov.horoscope;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.Date;

/**
 * Класс базы данных
 * Created by malkov on 28.09.2015.
 */
public class DB {
    final String TAG = "MyLog";

    private static DB mInstance = null;

    private DBHelper myDBHelper;
    public SQLiteDatabase myDB;
    private final Context myCtx;
    private static final String DB_NAME = "horoscope";
    private static final int DB_VERSION = 1;

    // Имена таблиц
    private static final String T_UPDATE = "update_horoscope";
    private static final String T_ZODIACS = "zodiacs";
    public static final String T_HOROSCOPE_PERIOD = "horoscope_period";
    private static final String T_HOROSCOPE_TYPE = "horoscope_type";
    public static final String T_HOROSCOPE = "horoscope";

    // Колонки
    public static final String C_ID = "_id";
    public static final String C_TIME = "time";
    public static final String C_ZODIAC = "zodiac";
    public static final String C_NAME = "name";
    public static final String C_PERIOD = "period";
    public static final String C_TEXT = "text";
    public static final String C_TYPE = "type";
    public static final String C_URL = "URL";
    public static final String C_ID_ZODIACS = "id_zodiacs";
    public static final String C_HOR_PERIOD = "id_hor_period";
    public static final String C_HOR_TYPE = "id_hor_type";

    // Создание таблиц
    private static final String CRT_UPDATE = " create table "
            + T_UPDATE + " ("
            + C_ID + " integer primary key autoincrement, "
            + C_TIME + " integer "
            + " ); ";
    private static final String CRT_ZODIACS = " create table "
            + T_ZODIACS + " ("
            + C_ID + " integer primary key autoincrement, "
            + C_ZODIAC + " text, "
            + C_NAME + " text, "
            + C_PERIOD + " text"
            + " ); ";
    private static final String CRT_HOROSCOPE_PERIOD = " create table "
            + T_HOROSCOPE_PERIOD + " ( "
            + C_ID + " integer primary key autoincrement, "
            + C_PERIOD + " text, "
            + C_NAME + " text, "
            + C_TIME + " integer "
            + " ); ";
    private static final String CRT_HOROSCOPE_TYPE = " create table "
            + T_HOROSCOPE_TYPE + " ("
            + C_ID + " integer primary key autoincrement, "
            + C_TYPE + " text, "
            + C_NAME + " text, "
            + C_URL + " text"
            + " ); ";
    private static final String CRT_HOROSCOPE = " create table "
            + T_HOROSCOPE + " ( "
            + C_ID_ZODIACS + " integer not null, "
            + C_HOR_PERIOD + " integer not null, "
            + C_HOR_TYPE + " integer not null, "
            + C_TEXT + " text, "
            + " unique( " + C_ID_ZODIACS + "," + C_HOR_PERIOD + "," + C_HOR_TYPE +") "
            + " );";


    private DB(Context ctx) {
        this.myCtx = ctx;
        myDBHelper = new DBHelper(ctx, DB_NAME, null, DB_VERSION);
        myDB = myDBHelper.getWritableDatabase();
    }

    public static DB getInstance(Context ctx){
        if(mInstance == null){
            mInstance = new DB(ctx.getApplicationContext());
        }
        return mInstance;
    }

    // Закрываем подключение
    public void close() {
        if (myDB != null) myDB.close();
        if (mInstance != null) mInstance = null;
    }

    /**
     * Заполняем таблицу Zodiacs
     *
     * @param db
     */
    private void fillZodiacs(SQLiteDatabase db) {
        String[] zodiacs = myCtx.getResources().getStringArray(R.array.zodiacs_en);
        String[] names = myCtx.getResources().getStringArray(R.array.zodiacs_ru);
        String[] zodiacsPeriod = myCtx.getResources().getStringArray(R.array.zodiacs_period);
        for (int i = 0; i < zodiacs.length; i++) {
            ContentValues cv = new ContentValues();
            cv.put(C_ZODIAC, zodiacs[i]);
            cv.put(C_NAME, names[i]);
            cv.put(C_PERIOD, zodiacsPeriod[i]);
            db.insert(T_ZODIACS, null, cv);
        }
    }

    /**
     * Заполняем таблицу horoscope_period
     *
     * @param db
     */
    private void fillHoroscopePeriod(SQLiteDatabase db) {
        String[] horoscopePeriod = myCtx.getResources().getStringArray(R.array.horoscope_period);
        String[] names = myCtx.getResources().getStringArray(R.array.horoscope_period_ru);
        for (int i = 0; i < horoscopePeriod.length; i++) {
            ContentValues cv = new ContentValues();
            cv.put(C_PERIOD, horoscopePeriod[i]);
            cv.put(C_NAME, names[i]);
            db.insert(T_HOROSCOPE_PERIOD, null, cv);
        }
    }

    /**
     * Заполняем таблицу horoscope_type
     *
     * @param db
     */
    private void fillHoroscopeType(SQLiteDatabase db) {
        String[] types = myCtx.getResources().getStringArray(R.array.horoscope_type);
        String[] names = myCtx.getResources().getStringArray(R.array.horoscope_type_ru);
        String[] urls = myCtx.getResources().getStringArray(R.array.horoscopes_url);
        for (int i = 0; i < types.length; i++) {
            ContentValues cv = new ContentValues();
            cv.put(C_TYPE, types[i]);
            cv.put(C_NAME, names[i]);
            cv.put(C_URL, urls[i]);
            db.insert(T_HOROSCOPE_TYPE, null, cv);
        }
    }

    /**
     * Получить ID знака зодиака по имени
     *
     * @param name
     * @return
     */
    public int findIDByNameZodiacs(String name){
        int id = 0;
        String selection = C_ZODIAC + " = ? ";
        String[] selectionArgs = new String[]{name};
        Cursor c = myDB.query(T_ZODIACS, null, selection, selectionArgs, null, null, null, "1");
        if(c.moveToFirst()){
            id = c.getInt(c.getColumnIndex(C_ID));
        }
        c.close();
        return id;
    }

    /**
     * Получить ID периода гороскопа
     *
     * @param name
     * @return
     */
    public int findIDByNameHoroscopePeriod (String name){
        int id = 0;
        String selection = C_PERIOD + " = ? ";
        String[] selectionArgs = new String[]{name};
        Cursor c = myDB.query(T_HOROSCOPE_PERIOD,null,selection,selectionArgs,null,null,null,"1");
        if(c.moveToFirst()){
            id = c.getInt(c.getColumnIndex(C_ID));
        }
        c.close();
        return id;
    }

    /**
     * Получаем курсор таблицы horoscope_type
     *
     * @return
     */
    public Cursor getHoroscopeType(){
        Cursor c = myDB.query(T_HOROSCOPE_TYPE,null,null,null,null,null,null);
        return c;
    }

    /**
     * Получаем курсор таблицы horoscope_type с колонками
     *
     * @return
     */
    public Cursor getHoroscopeType(String[] cols){
        Cursor c = myDB.query(T_HOROSCOPE_TYPE,cols,null,null,null,null,null);
        return c;
    }



    public long replace(String table, String nullColumnHack, ContentValues values) {
        return myDB.replace(table, nullColumnHack, values);
    }

    /**
     *  Ищем период по текущей дате
     * @param currentDate
     * @return
     */
    public String findPeriodByCurrentDate(Date currentDate){
        String period = "";
        long currentTime = currentDate.getTime()/1000;
        String time = String.valueOf(currentTime);

        String selection = C_TIME + " = ?";
        String[]  selectorArgs = new String[]{time};
        Cursor c = myDB.query(T_HOROSCOPE_PERIOD,null,selection,selectorArgs,null,null,null,"1");
        if(c.moveToFirst()){
            period = c.getString(c.getColumnIndex(C_PERIOD));
        }
        c.close();
        return period;
    }

    /**
     * Получаем гороскоп по имени зодиака и типу гороскопа
     * @param zodiac
     * @return
     */
    public Cursor getHoroscopeByZodiacHorType(String zodiac, String hot_type){
        Log.i(TAG,"-- getHoroscopeByZodiac");
        String table = T_ZODIACS + " z"
                + " join " + T_HOROSCOPE + " h on z." + C_ID + " = h." + C_ID_ZODIACS;
        String[] columns = {"h.id_hor_period","z.name", "h.text"};
        String selection = "z.zodiac = ? and h.id_hor_type = ?";
        String[] selectionAgrs = {zodiac, hot_type};
        String orderBy = "h.id_hor_period";

        Cursor c = null;
        c = myDB.query(table,columns,selection,selectionAgrs,null,null,orderBy);
        return c;
    }


    class DBHelper extends SQLiteOpenHelper {

        public DBHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
            super(context, name, factory, version);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.beginTransaction();
            // создаем таблицы
            db.execSQL(CRT_UPDATE);
            db.execSQL(CRT_ZODIACS);
            fillZodiacs(db);
            db.execSQL(CRT_HOROSCOPE_PERIOD);
            fillHoroscopePeriod(db);
            db.execSQL(CRT_HOROSCOPE_TYPE);
            fillHoroscopeType(db);
            db.execSQL(CRT_HOROSCOPE);
            db.setTransactionSuccessful();
            db.endTransaction();
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        }
    }
}
