package com.example.malkov.horoscope;

import android.app.Activity;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.SimpleCursorAdapter;
import android.widget.Spinner;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashMap;
import java.util.Map;


/**
 * Класс фрагмент для отображения гороскопа по знаку зодиака
 */
public class ForecastZodiacFragment extends Fragment {
    final String TAG = "MyLog";

    DB myDB = null;
    Cursor cHorType = null;
    Cursor cHoroscope = null;

    final int DEFAULT_HOR_TYPE = 1;
    String zodiac = null;

    public ForecastZodiacFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Получаем параметры
        if (getArguments() != null) {
            this.zodiac = getArguments().getString("ID");

            // Подключаемся к базе
            this.myDB = DB.getInstance(getActivity());

            // Получаем курсор тип гороскопа
            String[] columns = {myDB.C_ID,myDB.C_NAME};
            this.cHorType = myDB.getHoroscopeType(columns);

            // Получаем гороскоп
            this.cHoroscope = this.myDB.getHoroscopeByZodiacHorType(this.zodiac, String.valueOf(this.DEFAULT_HOR_TYPE));
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.fragment_forecast_zodiac, container, false);

        // Заполняем spinner из базы
        String[] aCols = {myDB.C_NAME};
        int[] adapterRowViews = new int[]{android.R.id.text1};
        SimpleCursorAdapter sa = new SimpleCursorAdapter(
                getActivity(),
                android.R.layout.simple_spinner_item,
                this.cHorType,
                aCols,
                adapterRowViews,
                0
        );
        sa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // Создание Spinner
        Spinner sp = (Spinner) v.findViewById(R.id.spinner);
        sp.setAdapter(sa);
        sp.setPrompt("Title");
        sp.setSelection(0);
        // Обработка выбора spinner
        sp.setOnItemSelectedListener(
                new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        Toast.makeText(getActivity(), "Position = " + position + " | ID = " + id, Toast.LENGTH_SHORT).show();
                        ForecastZodiacFragment.this.fillTabs(
                                v,
                                ForecastZodiacFragment.this.myDB.getHoroscopeByZodiacHorType(
                                        ForecastZodiacFragment.this.zodiac,
                                        String.valueOf(id)
                                )
                        );
                    }
                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {
                    }
                }
        );

        // Устанавливаем табы
        TabHost th = (TabHost) v.findViewById(R.id.tabHost);
        th.setup();

        String[] tagTab = {"tag1","tag2","tag3","tag4"};
        int[] resourcesNameTab = {R.string.yesterday,R.string.today,R.string.tomorrow,R.string.tomorrow02};
        int[] idTVTab = {R.id.tv,R.id.tv2,R.id.tv3,R.id.tv4};

        for(int i = 0; i < tagTab.length; i++){
            TabHost.TabSpec ts = th.newTabSpec(tagTab[i]);
            ts.setIndicator(getString(resourcesNameTab[i]));
            ts.setContent(idTVTab[i]);
            th.addTab(ts);
        }

        // Подставляем текст
        this.fillTabs(v,this.cHoroscope);

        return v;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        /*try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }*/
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public interface OnFragmentInteractionListener {
        public void onFragmentInteraction(Uri uri);
    }

    /**
     * Заполняем вкладки прогнозами
     * @param v
     * @param c
     */
    public void fillTabs(View v, Cursor c){
        // Находим TextView
        TextView tv = (TextView) v.findViewById(R.id.tv);
        TextView tv2 = (TextView) v.findViewById(R.id.tv2);
        TextView tv3 = (TextView) v.findViewById(R.id.tv3);
        TextView tv4 = (TextView) v.findViewById(R.id.tv4);

        // Пробегаемся по курсору и заполняем TextView
        if(c != null){
            if(c.moveToFirst()){
                Map<String,String> h = new HashMap<String,String>();
                do{
                    h.put(
                            c.getString(c.getColumnIndex("id_hor_period")),
                            c.getString(c.getColumnIndex("text"))
                    );
                }while (c.moveToNext());

                for(Map.Entry<String,String> entry : h.entrySet()){
                    switch (entry.getKey()) {
                        case "1":
                            tv.setText(entry.getValue());
                            break;
                        case "2":
                            tv2.setText(entry.getValue());
                            break;
                        case "3":
                            tv3.setText(entry.getValue());
                            break;
                        case "4":
                            tv4.setText(entry.getValue());
                            break;
                    }
                }
            }
        }
    }
}
